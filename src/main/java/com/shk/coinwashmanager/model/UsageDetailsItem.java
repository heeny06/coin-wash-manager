package com.shk.coinwashmanager.model;

import com.shk.coinwashmanager.entity.UsageDetails;
import com.shk.coinwashmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageDetailsItem {

    @ApiModelProperty(notes = "이용내역시퀀스")
    private Long usageDetailsId;

    @ApiModelProperty(notes = "이용시간")
    private LocalDateTime dateUsage;

    @ApiModelProperty(notes = "기계시퀀스")
    private Long machineId;

    @ApiModelProperty(notes = "기계전체이름")
    private String machineFullName;

    @ApiModelProperty(notes = "회원시퀀스")
    private Long memberId;

    @ApiModelProperty(notes = "회원이름")
    private String memberName;

    @ApiModelProperty(notes = "회원연락처")
    private String memberPhone;

    @ApiModelProperty(notes = "회원생일")
    private LocalDate memberBirthday;

    @ApiModelProperty(notes = "회원활성화여부")
    private Boolean memberIsEnable;

    @ApiModelProperty(notes = "회원가입일")
    private LocalDateTime memberDateJoin;

    @ApiModelProperty(notes = "회원탈퇴일")
    private LocalDateTime memberDateWithdrawal;

    private UsageDetailsItem(UsageDetailsItemBuilder builder) {
        this.usageDetailsId = builder.usageDetailsId;
        this.dateUsage = builder.dateUsage;
        this.machineId = builder.machineId;
        this.machineFullName = builder.machineFullName;
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.memberPhone = builder.memberPhone;
        this.memberBirthday = builder.memberBirthday;
        this.memberIsEnable = builder.memberIsEnable;
        this.memberDateJoin = builder.memberDateJoin;
        this.memberDateWithdrawal = builder.memberDateWithdrawal;
    }

    public static class UsageDetailsItemBuilder implements CommonModelBuilder<UsageDetailsItem> {

        private final Long usageDetailsId;
        private final LocalDateTime dateUsage;
        private final Long machineId;
        private final String machineFullName;
        private final Long memberId;
        private final String memberName;
        private final String memberPhone;
        private final LocalDate memberBirthday;
        private final Boolean memberIsEnable;
        private final LocalDateTime memberDateJoin;
        private final LocalDateTime memberDateWithdrawal;

        public UsageDetailsItemBuilder(UsageDetails usageDetails) {
            this.usageDetailsId = usageDetails.getId();
            this.dateUsage = usageDetails.getDateUsage();
            this.machineId = usageDetails.getMachine().getId();
            this.machineFullName = usageDetails.getMachine().getMachineType().getName() + " " + usageDetails.getMachine().getMachineName();
            this.memberId = usageDetails.getMember().getId();
            this.memberName = usageDetails.getMember().getMemberName();
            this.memberPhone = usageDetails.getMember().getMemberPhone();
            this.memberBirthday = usageDetails.getMember().getBirthday();
            this.memberIsEnable = usageDetails.getMember().getIsEnable();
            this.memberDateJoin = usageDetails.getMember().getDateJoin();
            this.memberDateWithdrawal = usageDetails.getMember().getDateWithdrawal();
        }

        @Override
        public UsageDetailsItem build() {
            return new UsageDetailsItem(this);
        }
    }
}
