package com.shk.coinwashmanager.exception;

public class CNoMemberDataException extends RuntimeException {
    /**
     * msg와 t를 인자로 필요로 하는 생성자 생성
     *
     * @param msg 예외처리 메시지
     * @param t   예외처리에 필요한 객체
     */
    public CNoMemberDataException(String msg, Throwable t) {
        super(msg, t); // 부모의 생성자에 msg, t 값을 인자로 주며 생성
    }

    /**
     * msg를 인자로 필요로 하는 생성자 생성
     *
     * @param msg 예외처리 메시지
     */
    public CNoMemberDataException(String msg) {
        super(msg); // 부모의 생성자에 msg 값을 인자로 주며 생성
    }

    /**
     * 빈 생성자 생성
     */
    public CNoMemberDataException() {
        super(); // 부모의 생성자를 생성
    }
}


