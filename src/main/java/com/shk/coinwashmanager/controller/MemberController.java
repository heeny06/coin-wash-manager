package com.shk.coinwashmanager.controller;

import com.shk.coinwashmanager.model.CommonResult;
import com.shk.coinwashmanager.model.ListResult;
import com.shk.coinwashmanager.model.MemberItem;
import com.shk.coinwashmanager.model.MemberJoinRequest;
import com.shk.coinwashmanager.service.MemberService;
import com.shk.coinwashmanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "회원 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {

    private final MemberService memberService;

    @ApiOperation(value = "회원 정보 등록")
    @PostMapping("/new")
    public CommonResult setMember(@RequestBody @Valid MemberJoinRequest joinRequest) {
        memberService.setMember(joinRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 리스트 조회")
    @GetMapping("/list")
    public ListResult<MemberItem> getMembers(@RequestParam(value = "isEnable", required = false) Boolean isEnable) {
        if (isEnable == null) return ResponseService.getListResult(memberService.getMembers(), true);
        else return ResponseService.getListResult(memberService.getMembers(isEnable), true);
    }

    @ApiOperation(value = "회원 정보 삭제")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "회원시퀀스", required = true)
    })
    @DeleteMapping("/{id}")
    public CommonResult delMember(@PathVariable long id) {
        memberService.putMemberWithdrawal(id);

        return ResponseService.getSuccessResult();
    }
}
