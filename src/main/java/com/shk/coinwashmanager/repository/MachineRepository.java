package com.shk.coinwashmanager.repository;

import com.shk.coinwashmanager.entity.Machine;
import com.shk.coinwashmanager.enums.MachineType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MachineRepository extends JpaRepository<Machine, Long> {
    List<Machine> findAllByMachineTypeOrderByDateUpdateDesc(MachineType machineType);
    List<Machine> findAllByOrderByDateUpdateDesc();
}
