package com.shk.coinwashmanager.entity;

import com.shk.coinwashmanager.enums.MachineType;
import com.shk.coinwashmanager.interfaces.CommonModelBuilder;
import com.shk.coinwashmanager.model.MachineNameUpdateRequest;
import com.shk.coinwashmanager.model.MachineRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)

public class Machine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private MachineType machineType;

    @Column(nullable = false, length = 15)
    private String machineName;

    @Column(nullable = false)
    private LocalDate datePurchase;

    @Column(nullable = false)
    private Double machinePrice;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putDataName(MachineNameUpdateRequest updateRequest) {
        this.machineName = updateRequest.getMachineName();
    }

    private Machine(MachineBuilder builder) {
        this.machineType = builder.machineType;
        this.machineName = builder.machineName;
        this.datePurchase = builder.datePurchase;
        this.machinePrice = builder.machinePrice;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class MachineBuilder implements CommonModelBuilder<Machine> {

        private final MachineType machineType;
        private  final  String machineName;
        private  final LocalDate datePurchase;
        private final Double machinePrice;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MachineBuilder(MachineRequest request) {
            this.machineType = request.getMachineType();
            this.machineName = request.getMachineName();
            this.datePurchase = request.getDatePurchase();
            this.machinePrice = request.getMachinePrice();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Machine build() {
            return new Machine(this);
        }
    }
}


