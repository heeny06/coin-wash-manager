package com.shk.coinwashmanager.service;

import com.shk.coinwashmanager.entity.Machine;
import com.shk.coinwashmanager.enums.MachineType;
import com.shk.coinwashmanager.exception.CMissingDataException;
import com.shk.coinwashmanager.model.*;
import com.shk.coinwashmanager.repository.MachineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor

public class MachineService {
    private final MachineRepository machineRepository;

    /**
     * 기계 등록
     *
     * @param request 등록하기 위한 기계 정보
     */
    public void setMachine(MachineRequest request) {
        Machine machine = new Machine.MachineBuilder(request).build();
        machineRepository.save(machine);
    }

    /**
     * 기계 원본 정보 가져오기
     *
     * @param id 기계 시퀀스
     * @return 기계 원본 정보
     */
    public Machine getMachineData(long id) {
        return machineRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    /**
     * 기계 상세정보 가져오기
     *
     * @param id 기계 시퀀스
     * @return 기계 상세정보
     */
    public MachineDetail getMachine(long id) {
        Machine machine = machineRepository.findById(id).orElseThrow(CMissingDataException::new);

        return new MachineDetail.MachineDetailBuilder(machine).build();
    }

    /**
     * 기계 정보 리스트 불러오기
     *
     * @return 기계 정보 리스트
     */
    public ListResult<MachineItem> getMachines() {
        List<Machine> machines = machineRepository.findAllByOrderByDateUpdateDesc();

        List<MachineItem> result = new LinkedList<>();

        machines.forEach(machine -> {
            MachineItem newItem = new MachineItem.MachineItemBuilder(machine).build();
            result.add(newItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 기계 종류에 따른 기계 정보 리스트 불러오기
     *
     * @param machineType 기계 종류
     * @return 기계 정보 리스트
     */
    public ListResult<MachineItem> getMachines(MachineType machineType) {
        List<Machine> machines = machineRepository.findAllByMachineTypeOrderByDateUpdateDesc(machineType);

        List<MachineItem> result = new LinkedList<>();

        machines.forEach(machine -> {
            MachineItem newItem = new MachineItem.MachineItemBuilder(machine).build();
            result.add(newItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 기계 이름 변경
     *
     * @param id 기계 시퀀스
     * @param updateRequest 변경된 기계 이름 정보
     */
    public void putMachineName(long id, MachineNameUpdateRequest updateRequest) {
        Machine machine = machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        machine.putDataName(updateRequest);
        machineRepository.save(machine);
    }

    /**
     * 기계 정보 삭제
     *
     * @param id 기계 시퀀스
     */
    public void delMachine(long id) {
        machineRepository.deleteById(id);
    }
}
