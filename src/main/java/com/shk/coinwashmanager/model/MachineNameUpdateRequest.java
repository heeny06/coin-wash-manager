package com.shk.coinwashmanager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MachineNameUpdateRequest {
    @ApiModelProperty(notes = "가계이름", required = true)
    @NotNull
    @Length(max = 15)
    private String machineName;
}
