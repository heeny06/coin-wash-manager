package com.shk.coinwashmanager.model;

import com.shk.coinwashmanager.entity.Machine;
import com.shk.coinwashmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MachineDetail {

    @ApiModelProperty(notes = "기계시퀀스")
    private Long id;

    @ApiModelProperty(notes = "기계종류")
    private String machineTypeName;

    @ApiModelProperty(notes = "기계이름")
    private String machineName;

    @ApiModelProperty(notes = "구매일")
    private LocalDate datePurchase;

    @ApiModelProperty(notes = "기계가격")
    private Double machinePrice;

    private MachineDetail(MachineDetailBuilder builder) {
        this.id = builder.id;
        this.machineTypeName = builder.machineTypeName;
        this.machineName = builder.machineName;
        this.datePurchase = builder.datePurchase;
        this.machinePrice = builder.machinePrice;
    }

    public static class MachineDetailBuilder implements CommonModelBuilder<MachineDetail> {

        private final Long id;
        private final String machineTypeName;
        private final String machineName;
        private final LocalDate datePurchase;
        private final Double machinePrice;

        public MachineDetailBuilder(Machine machine) {
            this.id = machine.getId();
            this.machineTypeName = machine.getMachineType().getName();
            this.machineName = machine.getMachineName();
            this.datePurchase = machine.getDatePurchase();
            this.machinePrice = machine.getMachinePrice();
        }
        @Override
        public MachineDetail build() {

            return new MachineDetail(this);
        }
    }
}
