package com.shk.coinwashmanager.service;

import com.shk.coinwashmanager.entity.Machine;
import com.shk.coinwashmanager.entity.Member;
import com.shk.coinwashmanager.entity.UsageDetails;
import com.shk.coinwashmanager.model.ListResult;
import com.shk.coinwashmanager.model.UsageDetailsItem;
import com.shk.coinwashmanager.repository.UsageDetailsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UsageDetailService {

    private final UsageDetailsRepository usageDetailsRepository;

    /**
     * 기계 이용내역 등록
     *
     * @param member 회원 정보
     * @param machine 기게 정보
     * @param dateUsage 이용 날짜
     */
    public void setUsageDetails(Member member, Machine machine, LocalDateTime dateUsage) {
        UsageDetails usageDetails = new UsageDetails.UsageDetailsBuilder(machine, member, dateUsage).build();
        usageDetailsRepository.save(usageDetails);
    }

    /**
     * 기간 내 이용내역 리스트 불러오기
     *
     * @param dateStart 조회 시작날
     * @param dateEnd 조회 마지막날
     * @return 이용내역 리스트
     */
    public ListResult<UsageDetailsItem> getUsageDetails(LocalDate dateStart, LocalDate dateEnd) {
        LocalDateTime dateStartTime = LocalDateTime.of(
                dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth(),
                0,
                0,
                0
        );

        LocalDateTime dateEndTime = LocalDateTime.of(
                dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth(),
                23,
                59,
                59
        );

        List<UsageDetails> usageDetails = usageDetailsRepository.findAllByDateUsageGreaterThanEqualAndDateUsageLessThanEqualOrderByIdDesc(dateStartTime, dateEndTime);

        List<UsageDetailsItem> result = new LinkedList<>();

        usageDetails.forEach(usageDetail -> {
            UsageDetailsItem newItem = new UsageDetailsItem.UsageDetailsItemBuilder(usageDetail).build();
            result.add(newItem);
        });

        return ListConvertService.settingResult(result);
    }
}
