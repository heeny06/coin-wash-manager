package com.shk.coinwashmanager.service;

import com.shk.coinwashmanager.entity.Member;
import com.shk.coinwashmanager.exception.CMissingDataException;
import com.shk.coinwashmanager.exception.CNoMemberDataException;
import com.shk.coinwashmanager.model.ListResult;
import com.shk.coinwashmanager.model.MachineItem;
import com.shk.coinwashmanager.model.MemberItem;
import com.shk.coinwashmanager.model.MemberJoinRequest;
import com.shk.coinwashmanager.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {

    private final MemberRepository memberRepository;

    /**
     * 회원 등록
     *
     * @param joinRequest 등록하기 위한 회원 정보
     */
    public void setMember(MemberJoinRequest joinRequest) {
        Member member = new Member.MemberBuilder(joinRequest).build();
        memberRepository.save(member);
    }

    /**
     * 회원 원본 정보 불러오기
     *
     * @param id 회원 시퀀스
     * @return 회원 원본 정보
     */
    public Member getMemberData(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    /**
     * 탈퇴 관계없이 전체 회원 정보 리스트를 불러온다
     *
     * @return 전체 회원 정보 리스트
     */
    public ListResult<MemberItem> getMembers() {
        List<Member> members = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberItem newItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(newItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 현재 회원 정보 리스트 또는 탈퇴한 회원 정보 리스트 불러오기
     *
     * @param isEnable 현재 회원 여부
     * @return 회원 정보 리스트
     */
    public ListResult<MemberItem> getMembers(boolean isEnable) {
        List<Member> members = memberRepository.findAllByIsEnableOrderByIdDesc(isEnable);

        List<MemberItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberItem newItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(newItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 회원 탈퇴
     *
     * @param id 회원 시퀀스
     */
    public void putMemberWithdrawal(long id) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);

        if (!member.getIsEnable()) throw new CNoMemberDataException();

        member.putWithdrawal();
        memberRepository.save(member);
    }
}
