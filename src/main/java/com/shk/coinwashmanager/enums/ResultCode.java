package com.shk.coinwashmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    // 상황에 따른 코드와 메시지를 관리
    SUCCESS(0, "성공하였습니다.") // 성공 코드와 메시지
    , FAILED(-1, "실패하였습니다.") // 실패 코드와 메시지

    , MISSING_DATA(-10000, "데이터를 찾을 수 없습니다.") // 데이터를 찾을 수 없는 예외 상황에 대한 코드와 메시지

    , NO_MEMBER_DATA(-20000, "회원정보가 없습니다.")
    ;

    private final Integer code; // 코드의 자료 형식과 이름
    private final String msg; // 메시지의 자료 형식과 이름
}

