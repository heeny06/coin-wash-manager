package com.shk.coinwashmanager.controller;

import com.shk.coinwashmanager.entity.Machine;
import com.shk.coinwashmanager.entity.Member;
import com.shk.coinwashmanager.model.CommonResult;
import com.shk.coinwashmanager.model.ListResult;
import com.shk.coinwashmanager.model.UsageDetailsItem;
import com.shk.coinwashmanager.model.UsageDetailsRequest;
import com.shk.coinwashmanager.service.MachineService;
import com.shk.coinwashmanager.service.MemberService;
import com.shk.coinwashmanager.service.ResponseService;
import com.shk.coinwashmanager.service.UsageDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "이용내역 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/usage-details")
public class UsageDetailsController {

    private final MemberService memberService;
    private final MachineService machineService;
    private final UsageDetailService usageDetailService;

    @ApiOperation(value = "이용내역 정보 등록")
    @PostMapping("/new")
    public CommonResult setUsageDetails(@RequestBody @Valid UsageDetailsRequest request) {
        Member member = memberService.getMemberData(request.getMemberId());
        Machine machine = machineService.getMachineData(request.getMachineId());
        usageDetailService.setUsageDetails(member, machine, request.getDateUsage());

        return ResponseService.getSuccessResult();
    }

    public ListResult<UsageDetailsItem> getUsageDetails(
            @RequestParam(value = "dateStart") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateStart,
            @RequestParam(value = "dateEnd") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateEnd
    ) {
        return ResponseService.getListResult(usageDetailService.getUsageDetails(dateStart, dateEnd), true);
    }
}

